package cn.lpq.magnetsearch;

import cn.lpq.magnetsearch.impl.AnidexMagentSearch;
import cn.lpq.magnetsearch.impl.BtstationMagentSearch;
import cn.lpq.magnetsearch.impl.BtyunsouMagentSearch;
import cn.lpq.magnetsearch.impl.TorrentbaMagentSearch;

public class MagentSearchFactory {

	public static AbstractMagentSearch build(int circuit) {
		AbstractMagentSearch magentSearch;
		switch (circuit) {
		case 1:
			magentSearch = new BtstationMagentSearch();
			break;
		case 2:
			magentSearch = new BtyunsouMagentSearch();
			break;
		case 3:
			magentSearch = new TorrentbaMagentSearch();
			break;
		case 4:
			magentSearch = new AnidexMagentSearch();
			break;
		default:
			magentSearch = new BtstationMagentSearch();
			break;
		}
		return magentSearch;
	}

}
